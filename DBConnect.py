# !/usr/bin/env python
# -*- coding: utf-8 -*-

import psycopg2
from dbConfig import *


class dbConnect:
    settings = {}

    def __init__(self):

        self.settings['host'] = db_host
        self.settings['user'] = db_user
        self.settings['port'] = db_port
        self.settings['database'] = db_name
        self.settings['password'] = db_password

        self.connection = None
        self.cursor = None
        self.connect()

    def connect(self):
        self.connection = psycopg2.connect(**self.settings)
        self.cursor = self.connection.cursor()

    def disconnect(self):
        self.connection.close()

    def commit(self):
        self.connection.commit()

    def query(self, sql, qtype='insert'):

        if qtype == 'insert':
            self.cursor.execute(sql)
            self.commit()
        elif qtype == 'select':
            self.cursor.execute(sql)
            data = self.cursor.fetchone()
            self.commit()
            return data


