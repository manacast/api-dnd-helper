# !/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request
from flask_cors import CORS

from DBConnect import dbConnect
from hashlib import sha256
import datetime

app = Flask(__name__)
cors = CORS(app)


@app.route('/register', methods=['POST'])
def register():
    try:
        new_one = request.json

        username = new_one['username']
        email = new_one['email']
        passwd_hash = new_one['passwd_hash']
        state = new_one['state']

        db = dbConnect()

        sql = "select username from users where username = '%s'" % username
        data = db.query(sql, 'select')

        if data is not None:
            return "логин занят"

        sql = "select email from users where email = '%s'" % email
        data = db.query(sql, 'select')

        if data is not None:
            return "email занят"

        sql = "select add_user('%s','%s','%s','%s')" % (username, email, passwd_hash, state)
        db.query(sql)
        db.disconnect()

        return "registration successful"

    except KeyError:
        return "что то пошло не так"


@app.route('/auth', methods=['POST'])
def authorization():
    new_one = request.json

    username = new_one['username']
    passwd_hash = new_one['passwd_hash']

    sql = "select * from users where  username = '%s' and passwd_hash = '%s'" % (username, passwd_hash)

    db = dbConnect()
    data = db.query(sql, 'select')

    if data is not None:
        token = gen_token(username)

        sql = "insert into auth values(default, '%s','%s', 'now()')" % (data[0], token)

        db.query(sql)
        db.disconnect()

        return '{"token" : "%s"}' % token
    else:
        db.disconnect()

        return "wrong login or password"


def gen_token(username):
    return sha256((username + str(datetime.datetime.now())).encode('utf-8')).hexdigest()


if __name__ == '__main__':
    app.run(debug=True)
